#ifndef __PROGTEST__
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <unistd.h>
#include <string>
#include <memory>
#include <vector>
#include <fstream>
#include <cassert>
#include <cstring>

#include <openssl/evp.h>
#include <openssl/rand.h>

using namespace std;

struct crypto_config
{
	const char * m_crypto_function;
	std::unique_ptr<uint8_t[]> m_key;
	std::unique_ptr<uint8_t[]> m_IV;
	size_t m_key_len;
	size_t m_IV_len;
};
#endif /* _PROGTEST_ */

#define INBUFF_CAP 1024
#define OUTBUFF_CAP ( INBUFF_CAP + EVP_MAX_BLOCK_LENGTH )

class FileCryptor {
public:
    FileCryptor(const std::string& in_filename, const std::string& out_filename, crypto_config& config)
            : in_filename_(in_filename), out_filename_(out_filename), config_(config), ctx_(NULL), cipher_(NULL) {}

    bool encrypt_data() {
        return crypt(true);
    }

    bool decrypt_data() {
        return crypt(false);
    }

    virtual ~FileCryptor();

private:
    std::string in_filename_;
    std::string out_filename_;
    crypto_config& config_;
    std::ifstream ifs_;
    std::ofstream ofs_;
    EVP_CIPHER_CTX* ctx_;
    const EVP_CIPHER* cipher_;

    bool validateConfig(bool encrypt);
    bool updateFile();
    bool initContext(bool encrypt);

    bool crypt(bool encrypt);

    friend bool copyHeader(FileCryptor& fc);
};


bool FileCryptor::validateConfig(bool encrypt) {
    size_t cipherKeyLen = EVP_CIPHER_key_length(cipher_);
    size_t cipherIVLen = EVP_CIPHER_iv_length(cipher_);
    if (config_.m_key == nullptr || config_.m_key_len < cipherKeyLen) {
        if (!encrypt)
            return false;
        config_.m_key.reset(new uint8_t[cipherKeyLen]);
        config_.m_key_len = cipherKeyLen;
        RAND_bytes(config_.m_key.get(), cipherKeyLen);
    }
    if (config_.m_IV == nullptr || config_.m_IV_len < cipherIVLen) {
        if (cipherIVLen) {
            if (!encrypt)
                return false;
            config_.m_IV.reset(new uint8_t[cipherIVLen]);
            config_.m_IV_len = cipherIVLen;
            RAND_bytes(config_.m_IV.get(), cipherIVLen);
        }
    }
    return true;
}

bool FileCryptor::updateFile() {
    char inBuff[INBUFF_CAP] = {};
    char outBuff[OUTBUFF_CAP] = {};
    int outSize = 0;
    while (ifs_.good() && ofs_.good()) {
        ifs_.read(inBuff, INBUFF_CAP);
        if (!EVP_CipherUpdate(ctx_,
                              reinterpret_cast<unsigned char*>(outBuff), &outSize,
                              reinterpret_cast<const unsigned char*>(inBuff), ifs_.gcount()))
            return false;
        ofs_.write(outBuff, outSize);
    }
    // finished reading infile
    if (ifs_.eof()) {
        if (!EVP_CipherFinal_ex(ctx_, reinterpret_cast<unsigned char*>(outBuff), &outSize))
            return false;
        ofs_.write(outBuff, outSize);
        if (!ofs_.good())
            return false;
        return true;
    }
    return false;
}

bool FileCryptor::initContext(bool encrypt) {
    OpenSSL_add_all_ciphers();
    ctx_ = EVP_CIPHER_CTX_new();
    if (!ctx_)
        return false;
    cipher_ = EVP_get_cipherbyname(config_.m_crypto_function);
    if (!cipher_)
        return false;
    if (!validateConfig(encrypt))
        return false;
    if (!EVP_CipherInit_ex(ctx_, cipher_, NULL, config_.m_key.get(), config_.m_IV.get(), static_cast<int>(encrypt)))
        return false;
    return true;
}

bool FileCryptor::crypt(bool encrypt) {
    ifs_.open(in_filename_, std::ios::binary);
    ofs_.open(out_filename_, std::ios::binary);
    if (!ifs_.good() || !ofs_.good()) {
        std::cout << "Unable to open file" << std::endl;
        return false;
    }
    if (!copyHeader(*this)) {
        std::cout << "Unable to copy header" << std::endl;
        return false;
    }
    if (!initContext(encrypt) || !updateFile()) {
        std::cout << "Cipher failed" << std::endl;
        return false;
    }
    return true;
}

bool copyHeader(FileCryptor& fc) {
    char header[18] = {};
    fc.ifs_.read(header, 18);
    if (fc.ifs_.gcount() != 18) {
        std::cout << "Unable to read header" << std::endl;
        return false;
    }
    fc.ofs_.write(header, 18);
    if (!fc.ofs_.good())
        return false;
    return true;
}

FileCryptor::~FileCryptor() {
    EVP_CIPHER_CTX_free(ctx_);
}


bool encrypt_data ( const std::string & in_filename, const std::string & out_filename, crypto_config & config ){
    FileCryptor fc(in_filename, out_filename, config);
    return fc.encrypt_data();
}

bool decrypt_data ( const std::string & in_filename, const std::string & out_filename, crypto_config & config ){
    FileCryptor fc(in_filename, out_filename, config);
    return fc.decrypt_data();
}


#ifndef __PROGTEST__

#include <filesystem>
bool compare_files ( const char * name1, const char * name2 ) {
    namespace fs = std::filesystem;
    if ( fs::file_size(name1) != fs::file_size(name2) ) {
        cout << "File size mismatch" << endl;
        return false;
    }
    ifstream ifs1 (name1);
    ifstream ifs2 (name2);
    string word;
    string word2;
    while ( ifs1 >> word && ifs2 >> word2 ) {
        if ( word != word2 ) {
            cout << "Files not equal" << endl;
            return false;
        }
    }
    return true;
}
int main ( void )
{
    crypto_config config {nullptr, nullptr, nullptr, 0, 0};

    // ECB mode
    config.m_crypto_function = "AES-128-ECB";
    config.m_key = std::make_unique<uint8_t[]>(16);
    memset(config.m_key.get(), 0, 16);
    config.m_key_len = 16;

    assert( encrypt_data  ("homer-simpson.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "homer-simpson_enc_ecb.TGA") );

    assert( decrypt_data  ("homer-simpson_enc_ecb.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "homer-simpson.TGA") );

    assert( encrypt_data  ("UCM8.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "UCM8_enc_ecb.TGA") );

    assert( decrypt_data  ("UCM8_enc_ecb.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "UCM8.TGA") );

    assert( encrypt_data  ("image_1.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "ref_1_enc_ecb.TGA") );

    assert( encrypt_data  ("image_2.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "ref_2_enc_ecb.TGA") );

    assert( decrypt_data ("image_3_enc_ecb.TGA", "out_file.TGA", config)  &&
            compare_files("out_file.TGA", "ref_3_dec_ecb.TGA") );

    assert( decrypt_data ("image_4_enc_ecb.TGA", "out_file.TGA", config)  &&
            compare_files("out_file.TGA", "ref_4_dec_ecb.TGA") );

    // CBC mode
    config.m_crypto_function = "AES-128-CBC";
    config.m_IV = std::make_unique<uint8_t[]>(16);
    config.m_IV_len = 16;
    memset(config.m_IV.get(), 0, 16);

    assert( encrypt_data  ("UCM8.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "UCM8_enc_cbc.TGA") );

    assert( decrypt_data  ("UCM8_enc_cbc.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "UCM8.TGA") );

    assert( encrypt_data  ("homer-simpson.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "homer-simpson_enc_cbc.TGA") );

    assert( decrypt_data  ("homer-simpson_enc_cbc.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "homer-simpson.TGA") );

    assert( encrypt_data  ("image_1.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "ref_5_enc_cbc.TGA") );

    assert( encrypt_data  ("image_2.TGA", "out_file.TGA", config) &&
            compare_files ("out_file.TGA", "ref_6_enc_cbc.TGA") );

    assert( decrypt_data ("image_7_enc_cbc.TGA", "out_file.TGA", config)  &&
            compare_files("out_file.TGA", "ref_7_dec_cbc.TGA") );

    assert( decrypt_data ("image_8_enc_cbc.TGA", "out_file.TGA", config)  &&
            compare_files("out_file.TGA", "ref_8_dec_cbc.TGA") );
    return 0;
}

#endif /* _PROGTEST_ */
