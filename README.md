# Progtest_02
# Šifrování a dešifrování obrázkového souboru ve formátu TGA

Vaším úkolem je realizovat dvě (či více) funkce, které dokáží zašifrovat a dešifrovat obrázkový soubor ve formátu TGA.

## Zjednodušená forma obrázku

Povinná hlavička: 18 bytů - tyto bajty nijak nemodifikujeme, jen je překopírujeme do zašifrovaného obrázku.

Volitelná část hlavičky: velikost se vypočítá z povinné části hlavičky - tuto část hlavičky budeme považovat za obrázková data, tj. beze změn je zašifrujeme společně s obrázkovými daty.

Obrázková data: zbytek.

## Parametry implementovaných funkcí

- `bool encrypt_data(const string &in_filename, const string &out_filename, crypto_config &config)`: Funkce pro šifrování souboru.
  - `in_filename`: vstupní jméno souboru.
  - `out_filename`: výstupní jméno souboru.
  - `config`: datová struktura `crypto_config` popsaná níže.
  - Návratová hodnota je true v případě úspěchu, false v opačném případě.

- `bool decrypt_data(const string &in_filename, const string &out_filename, crypto_config &config)`: Funkce pro dešifrování souboru.
  - `in_filename`: vstupní jméno souboru.
  - `out_filename`: výstupní jméno souboru.
  - `config`: datová struktura `crypto_config` popsaná níže.
  - Návratová hodnota je true v případě úspěchu, false v opačném případě.

## Struktura crypto_config

- Zvolená bloková šifra zadaná pomocí jejího názvu.
- Tajný šifrovací klíč a jeho velikost.
- Inicializační vektor (IV) a jeho velikost.

## Poznámky

- Pokud je šifrovací klíč (či IV) nedostačující, musí dojít k jejich bezpečnému vygenerování.
- Ve výchozím stavu mají blokové šifry zapnuté zarovnání (padding).
- Funkce pro šifrování a dešifrování využijte funkce z OpenSSL.
- Při kompilaci na Progtestu můžete být omezeni nejen časem, ale i velikostí dostupné paměti.
- Odevzdávejte zdrojový soubor, který obsahuje implementaci požadovaných funkcí. Do zdrojového souboru si můžete přidat i další podpůrné funkce.
